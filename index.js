const express = require("express");

const app = express();
const PORT = process.env.PORT || 56201;

app.use(express.text());

// Квадрат числа
app.post("/square", (req, res) => {
  const number = parseFloat(req.body);
  if (isNaN(number)) {
    return res.status(400).json({ error: "Invalid number" });
  }
  const square = number * number;
  res.json({ number, square });
});

// Реверс тексту
app.post("/reverse", (req, res) => {
  const reversedText = req.body.split("").reverse().join("");
  res.send(reversedText);
});

// Дані про дату
app.get("/date/:year/:month/:day", (req, res) => {
  const { year, month, day } = req.params;
  const date = new Date(year, month - 1, day);

  if (isNaN(date.getTime())) {
    return res.status(400).json({ error: "Invalid date" });
  }

  const weekDay = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][date.getDay()];
  const isLeapYear = (year % 4 === 0 && year % 100 !== 0) || (year % 400 === 0);
  const today = new Date();
  const difference = Math.abs(Math.floor((today - date) / (1000 * 60 * 60 * 24)));

  res.json({ weekDay, isLeapYear, difference });
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});